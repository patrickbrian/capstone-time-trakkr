package com.timetrakkr

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class TimeTrakkrApplication

fun main(args: Array<String>) {
	runApplication<TimeTrakkrApplication>(*args)
}
